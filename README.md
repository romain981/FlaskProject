# Projet Flask

## Qui sommes-nous

Nous sommes trois élèves de l'IUT d'Orléans, en seconde année au
département informatique.<br>
Nous sommes donc:
 * IDIR Marc
 * LOPEZ Romain
 * RAPINE Bastian

## Quel est ce projet

Il s'agit d'un projet de groupe réalisé dans le cadre de nos études,
pour notre cours de développement web.<br>
Nous devons donc créer une application web avec Flask offrant aux
utilisateurs de manipuler dynamiquement une base de données, selon
la table de notation indiquée [plus bas](#table-de-notation).

## Utilisation du produit

Afin de lancer l'application web, produit de nos efforts, nous avons
créé un script permettant aisément autant la mise en place d'un
environnement virtuel utilisant Python, mais aussi le lancement de
l'application en utilisant un port au choix.<br>
Il suffit donc de lancer le script `server.sh` sous Linux, ou
`server.bat` sous Windows afin de lancer l'application. (attention!
Certaines versions de Windows peuvent ne pas supporter ce script!)<br>
L'ajout de l'argument `open` lors du lancement d'un des scripts permet
de lancer le serveur en broadcast, permettant ainsi de l'ouvrir au
réseau local, sinon à l'internet entier (si la configuration du réseau
le permet).

# Notation

## Soutenance

5 à 10 minutes<br>
Par groupe de 3<br>
Git en maintainer

## Table de notation

| Element                                          | Points |
|--------------------------------------------------|--------|
| 2 classes (ManyToMany) base sqlite3              | 2      |
| Création, Affichage, Edition, Suppretion (CRUD)  | 2      |
| Recherche                                        | 2      |
| Bootstrap (ou semblabe) ou beau CSS              | 2      |
| Login                                            | 2      |
| Inscription                                      | 2      |
| Blocage pages par login                          | 2      |
|                                                  |        |
| Playlist par utilisateur (ou adapté au sujet)    | 2      |
| Notation album (ou élément du sujet) et moyennes | 2      |
| Toute autre fonctionnalité intéressante          | 2      |

## Mail de contact

[Sylvain Austruy](mailto:sylvain.austruy@univ-orleans.fr)
