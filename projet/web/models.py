import sys

from .app import db, login_manager, check_username
from .constants import *
from flask_login import UserMixin
from typing import Tuple, List


# templates for Typing definition
class PokedexMove:
    pass


def get_user(username):
    return User.query.filter(User.username == username).one_or_none()


def get_pokemon(pokemon_id):
    return Pokemon.query.filter(Pokemon.pokeId == pokemon_id).one_or_none()


def get_team(team_id):
    return Team.query.get(team_id)


@login_manager.user_loader
def load_user(username):
    return User.query.get(username)


class User(db.Model, UserMixin):
    userId = db.Column('id', db.Integer, primary_key=True)
    username = db.Column('username', db.Text, nullable=False, unique=True)
    passwordHash = db.Column('passwordHash', db.Text, nullable=False)

    def get_id(self):
        return self.userId

    def get_teams(self) -> list:
        teams = Team.query.all()
        r = []
        for t in teams:
            if t.ownerId == self.userId:
                r.append(t)
        return r

    @staticmethod
    def check_name(name: str) -> Tuple[int, str]:
        i = check_username(name)
        user = get_user(name)
        if i == 1:
            return i, "Username too short (minimum 5 characters)"
        if i == 2:
            return i, "Username too long (maximum 25 characters)"
        if i == 3:
            return i, "Invalid username: only a-z/A-Z, digits and underscores are accepted"
        if i == 4:
            return i, "Invalid username: at least 3 letters are required"
        if user:
            return 5, "Pseudo déjà existant !"
        return i, ""


class Pokemon(db.Model):
    pokeId = db.Column('pokeId', db.Integer, primary_key=True)
    ownerId = db.Column(db.Integer, db.ForeignKey(User.userId), nullable=False)
    pokedexId = db.Column('pokedexId', db.Integer, nullable=False)
    pokeName = db.Column('pokeName', db.Text)
    pokeLevel = db.Column('pokeLevel', db.Integer, nullable=False)
    move1 = db.Column('move1', db.Integer, nullable=False)
    move2 = db.Column('move2', db.Integer, nullable=False)
    move3 = db.Column('move3', db.Integer, nullable=False)
    move4 = db.Column('move4', db.Integer, nullable=False)

    def get_entry_name(self) -> str:
        """
        Retourne le nom de l'espèce du pokémon
        """
        r = Constants.DB_POKEMON.execute(f"select identifier from pokemon where id = {self.pokedexId}")
        for row in r:
            return str(row[0]).title()

    def get_move1(self):
        if self.move1 is None:
            return None
        return PokedexMove.get_move(self.move1)

    def get_move2(self):
        if self.move2 is None:
            return None
        return PokedexMove.get_move(self.move2)

    def get_move3(self):
        if self.move3 is None:
            return None
        return PokedexMove.get_move(self.move3)

    def get_move4(self):
        if self.move4 is None:
            return None
        return PokedexMove.get_move(self.move4)


class Team(db.Model):
    teamId = db.Column('teamId', db.Integer, primary_key=True)
    ownerId = db.Column(db.Integer, db.ForeignKey(User.userId), nullable=False)
    teamName = db.Column('teamName', db.Text, nullable=False)
    pokemon1 = db.Column(db.Integer, db.ForeignKey(Pokemon.pokeId), nullable=True)
    pokemon2 = db.Column(db.Integer, db.ForeignKey(Pokemon.pokeId), nullable=True)
    pokemon3 = db.Column(db.Integer, db.ForeignKey(Pokemon.pokeId), nullable=True)
    pokemon4 = db.Column(db.Integer, db.ForeignKey(Pokemon.pokeId), nullable=True)
    pokemon5 = db.Column(db.Integer, db.ForeignKey(Pokemon.pokeId), nullable=True)
    pokemon6 = db.Column(db.Integer, db.ForeignKey(Pokemon.pokeId), nullable=True)

    owner = db.relationship('User', foreign_keys='Team.ownerId')
    p1 = db.relationship('Pokemon', foreign_keys='Team.pokemon1')
    p2 = db.relationship('Pokemon', foreign_keys='Team.pokemon2')
    p3 = db.relationship('Pokemon', foreign_keys='Team.pokemon3')
    p4 = db.relationship('Pokemon', foreign_keys='Team.pokemon4')
    p5 = db.relationship('Pokemon', foreign_keys='Team.pokemon5')
    p6 = db.relationship('Pokemon', foreign_keys='Team.pokemon6')

    def count_pokemons(self) -> int:
        i = 0
        if self.pokemon1 is not None:
            i += 1
        if self.pokemon2 is not None:
            i += 1
        if self.pokemon3 is not None:
            i += 1
        if self.pokemon4 is not None:
            i += 1
        if self.pokemon5 is not None:
            i += 1
        if self.pokemon6 is not None:
            i += 1
        return i

    def remove_pokemon(self, pokid: int) -> bool:
        if self.pokemon1 and self.p1.pokeId == pokid:
            self.pokemon1 = None
        elif self.pokemon2 and self.p2.pokeId == pokid:
            self.pokemon2 = None
        elif self.pokemon3 and self.p3.pokeId == pokid:
            self.pokemon3 = None
        elif self.pokemon4 and self.p4.pokeId == pokid:
            self.pokemon4 = None
        elif self.pokemon5 and self.p5.pokeId == pokid:
            self.pokemon5 = None
        elif self.pokemon6 and self.p6.pokeId == pokid:
            self.pokemon6 = None
        else:
            return False
        db.session.commit()
        return True

    @staticmethod
    def get_team_with_pokemon(pokid: int):
        team = db.engine.execute(f"select teamId from Team where pokemon1 = {pokid}").one_or_none()
        if team is None:
            team = db.engine.execute(f"select teamId from Team where pokemon2 = {pokid}").one_or_none()
        if team is None:
            team = db.engine.execute(f"select teamId from Team where pokemon3 = {pokid}").one_or_none()
        if team is None:
            team = db.engine.execute(f"select teamId from Team where pokemon4 = {pokid}").one_or_none()
        if team is None:
            team = db.engine.execute(f"select teamId from Team where pokemon5 = {pokid}").one_or_none()
        if team is None:
            team = db.engine.execute(f"select teamId from Team where pokemon6 = {pokid}").one_or_none()
        if team is None:
            return None
        return get_team(team)

    @staticmethod
    def check_name(name: str) -> Tuple[int, str]:
        i = check_username(name)
        if i == 1:
            return i, "Team name too short (minimum 5 characters)"
        if i == 2:
            return i, "Team name too long (maximum 25 characters)"
        if i == 3:
            return i, "Invalid team name: only a-z/A-Z, digits and underscores are accepted"
        if i == 4:
            return i, "Invalid team name: at least 3 letters are required"
        return i, ""


class PokedexEntry:
    id: int
    name: str
    height: float
    weight: float
    types: list

    def __init__(self, id: int, name: str, height: int, weight: int, types: list):
        self.id = id
        self.name = name
        self.height = height/10
        if self.height == int(self.height):
            self.height = int(self.height)
        self.weight = weight/10
        if self.weight == int(self.weight):
            self.weight = int(self.weight)
        self.types = types

    def get_learnable_moves(self) -> List[PokedexMove]:
        """
        Retourne la liste des attaques que peut apprendre le pokemon
        """
        attaques = []
        r = Constants.DB_POKEMON.execute(f"select m.id, m.identifier, t.identifier, m.pp, m.power, m.accuracy from\
            moves m left join (select move_id, pokemon_id from pokemon_moves) p on p.move_id = m.id left join\
            (select id, identifier from types) t on t.id = m.type_id where p.pokemon_id = {self.id} group by m.id\
            order by t.id asc, m.power desc, m.accuracy desc")
        for row in r:
            attaques.append(PokedexMove(row[0], row[1], row[2], row[3], row[4], row[5]))
        return attaques

    def get_learnable_moves_types(self, types: dict) -> List[PokedexMove]:
        """
        Retourne la liste des attaques que peut apprendre le pokemon,
        en fonction du dictionnaire (nom de type-bool) donné
        """
        attaques = []
        r = Constants.DB_POKEMON.execute(f"select m.id, m.identifier, t.identifier, m.pp, m.power, m.accuracy from\
            moves m left join (select move_id, pokemon_id from pokemon_moves) p on p.move_id = m.id left join\
             (select id, identifier from types) t on t.id = m.type_id where p.pokemon_id = {self.id} group by m.id\
              order by t.id asc, m.power desc, m.accuracy desc")
        for row in r:
            if types[row[2]] == "true":
                attaques.append(PokedexMove(row[0], row[1], row[2], row[3], row[4], row[5]))
        return attaques

    @staticmethod
    def get_pokemon(id: int):
        # TODO improve using species ID -> implement pokemon shapes & mega evolutions
        r = Constants.DB_POKEMON.execute(f"select id, identifier, height, weight from pokemon where id = {id}")
        for row in r:
            return PokedexEntry(row[0], row[1], row[2], row[3], PokedexEntry.get_types_pokemon(id))
        return None

    @staticmethod
    def get_pokedex():
        pokedex = []
        r = Constants.DB_POKEMON.execute(f"select id, identifier, height, weight from pokemon")
        for row in r:
            pokedex.append(PokedexEntry(row[0], row[1], row[2], row[3], PokedexEntry.get_types_pokemon(row[0])))
        return pokedex

    @staticmethod
    def get_pokedex_range(from_id: int, to_id: int) -> list:
        range_dex = []
        if not to_id > from_id > 0:
            return range_dex
        r = Constants.DB_POKEMON.execute(f"select id, identifier, height, weight from pokemon order by id")
        has_next = False
        i = 0
        for row in r:
            i += 1
            if i < from_id:
                continue
            if i > to_id:
                has_next = True
                break
            range_dex.append(PokedexEntry(row[0], row[1], row[2], row[3], PokedexEntry.get_types_pokemon(row[0])))
        range_dex.append(has_next)
        return range_dex

    @staticmethod
    def get_types_pokemon(id: int) -> list:
        """
        Obtient type (type_id, nom_type) avec l'id d'un pokemon
        """
        types = []
        r = Constants.DB_POKEMON.execute(
            f"select id, identifier from types where id in( select type_id from pokemon_types where pokemon_id = {id})")
        for row in r:
            types.append((row[0], row[1]))
        return types

    @staticmethod
    def get_moves_types_pokemon(id: int):
        """
        Obtient toutes les attaques des types du pokemon.
        Exemple si le pokemon est feu et vol, cela recupere toutes les attaques feu et toutes les attaques vol
        """
        types = PokedexEntry.get_types_pokemon(id)
        res = []
        for t in types:
            res.append(PokedexMove.get_moves_type(t[0]))
        print(res)
        return res

    @staticmethod
    def search_pokemon(research: str) -> list:
        """
        Retourne une liste des pokemons pouvant correspondre à la recherche donnée
        """
        search = research.lower()
        result = []
        r = Constants.DB_POKEMON.execute(
            f"select id, identifier, height, weight from pokemon where identifier like \"%{search}%\" or identifier\
            like \"{search}%\" or identifier like \"%{search}\" or identifier like \"{search}\" or cast(id as varchar)\
            like \"%{search}%\" or cast(id as varchar) like \"{search}%\" or cast(id as varchar) like \"%{search}\" or\
            cast(id as varchar) like \"{search}\" order  by id")
        for row in r:
            result.append(PokedexEntry(row[0], row[1], row[2], row[3], PokedexEntry.get_types_pokemon(row[0])))
        return result

    @staticmethod
    def get_single_pokemon(research: str) -> int:
        """
        Retourne l'ID un unique pokemon correspondant à la recherche, 0 si plusieurs ou aucun
        """
        search = research.lower()
        i = out = 0
        r = Constants.DB_POKEMON.execute(f"select id from pokemon where identifier like \"{search}\" \
        or cast(id as varchar) like \"{search}\" order  by id")
        for row in r:
            if i != 0:
                return 0
            out = row[0]
            i += 1
        return out
    
    @staticmethod
    def get_pokemons_type(id: int) -> list:
        """
        Obtient tous les pokemons d'un type
        """
        pokedex_type = []
        r = Constants.DB_POKEMON.execute(
            f"select id, identifier, height weight from pokemon where id in \
            (select id from pokemon_types where pokemon_id = {id})")
        for row in r:
            pokedex_type.append((row[0], row[1]))
        return pokedex_type


    @staticmethod
    def get_pokedex_for_type(id: int) -> list:
        out = []
        r = Constants.DB_POKEMON.execute(f"select id, identifier, height, weight from pokemon where id \
        in( select pokemon_id from pokemon_types where type_id = {id} order by type_id)")
        for row in r:
            out.append(PokedexEntry(row[0], row[1], row[2], row[3], PokedexEntry.get_types_pokemon(row[0])))
        return out

    @staticmethod
    def get_type(id: int) -> list:
        """Obtient le nom du type avec son ID"""
        type_name = []
        r = Constants.DB_POKEMON.execute(
            f"select identifier from types where id = {id}")
        for row in r:
            type_name.append(row[0])
        return type_name


class PokedexMove:
    id: int
    name: str
    type: str
    pp: int
    power: int
    accuracy: int

    def __init__(self, id: int, name: str, type: str, pp: int, power: int, accuracy: int):
        self.id = id
        self.name = name
        self.type = type
        self.pp = pp
        self.power = power
        self.accuracy = accuracy

    def __repr__(self) -> str:
        return f"{'{'}\"id\":{self.id},\"name\":\"{self.name}\",\"type\":\"{self.type}\",\"pp\":{self.pp},\"power\":" +\
               f"\"{self.power}\",\"acc\":\"{self.accuracy}\",\"type_id\":{PokedexMove.get_move_id(self.type)}{'}'}"

    @staticmethod
    def get_move(id: int):
        """
        Obtient un move avec son id
        """
        r = Constants.DB_POKEMON.execute(
            f"select id, identifier, pp, type_id, power, accuracy from moves where id = {id}")
        for row in r:
            r2 = Constants.DB_POKEMON.execute(f"select identifier from types where id = {row[3]}")
            for row2 in r2:
                return PokedexMove(row[0], row[1], row2[0], row[2], row[4], row[5])
        return None

    @staticmethod
    def get_moves_type(id: int) -> list:
        """
        Obtient tout les moves avec l'id d'un type
        """
        moves = []
        r = Constants.DB_POKEMON.execute(
            f"select id, identifier, pp, type_id, power, accuracy from moves where type_id = {id} order by power")
        for row in r:
            r2 = Constants.DB_POKEMON.execute(f"select identifier from types where id = {id}")
            for row2 in r2:
                moves.append(PokedexMove(row[0], row[1], row2[0], row[2], row[4], row[5]))
        return moves

    @staticmethod
    def get_move_id(s: str) -> int:
        """
        Retourne l'ID du type nommé
        """
        r = Constants.DB_POKEMON.execute("select id, identifier from types")
        for row in r:
            if row[1] == s:
                return row[0]
