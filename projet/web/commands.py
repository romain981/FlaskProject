import click
from .app import app, db
from .models import *


@app.cli.command()
def syncdb():
    db.create_all()

@app.cli.command()
@click.argument('username')
@click.argument('password')
def newuser(username, password):
    """Adds a new user"""
    from .models import User
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = User(username=username, passwordHash=m.hexdigest())
    # cf. views.signup
    db.session.add(u)
    db.session.commit()


@app.cli.command()
@click.argument('username')
@click.argument('password')
def passwd(username, password):
    """Changes password"""

    from .models import User
    from hashlib import sha256
    m = sha256()
    m.update(password.encode())
    u = get_user(username)
    if u is not None:
        u.passwordHash = m.hexdigest()
        db.session.commit()
