from .constants import Constants
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, current_user
from functools import wraps
from werkzeug.routing import RequestRedirect, MethodNotAllowed, NotFound

import logging

app = Flask(__name__, template_folder="../resources/templates", static_folder="../resources/static")

log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///../resources/db/site.sqlite"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["POKEDEX_DB_CONNECT"] = Constants.DB_POKEMON

app.config['SECRET_KEY'] = "8d939a9d-2d7e-4261-b440-4d8706877b83"

login_manager = LoginManager(app)
login_manager.login_view = "/?l=0"

db = SQLAlchemy(app)


def get_view_function(url, method='GET'):
    """Match a url and return the view and arguments
    it will be called with, or None if there is no view.
    """

    adapter = app.url_map.bind('localhost')

    try:
        match = adapter.match(url, method=method)
    except RequestRedirect as e:
        # recursively match redirects
        return get_view_function(e.new_url, method)
    except (MethodNotAllowed, NotFound):
        # no match
        return None

    try:
        # return the view function and arguments
        return app.view_functions[match[0]], match[1]
    except KeyError:
        # no view is associated with the endpoint
        return None


def logout_required(func):
    """
    Shall ensure that any logged in a view decorated with it get redirected to
    the given page before calling the view, so that only logged out users can
    access the actual view. For example::

        @app.route('/route')
        @logout_required
        def route_view():
            return "Hello World!"

    See :ref:`login_required`

    :param func: The view function to decorate
    :type func: function
    """
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if current_user.is_authenticated:
            return get_view_function("/")[0](*args, **kwargs)
        else:
            return func(*args, **kwargs)
    return decorated_view


def check_username(username: str) -> int:
    if len(username) < 5:
        return 1
    if len(username) > 25:
        return 2
    letters = {"letters": 0, "numbers": 0, "underscores": 0, "others": 0}
    for c in username:
        asc = ord(c)
        if 47 < asc < 58:
            letters["numbers"] += 1
        elif 64 < asc < 91 or 96 < asc < 123:
            letters["letters"] += 1
        elif asc == 95:
            letters["underscores"] += 1
        else:
            letters["others"] += 1
    if letters["others"] > 1:
        return 3
    if letters["letters"] < 3:
        return 4
    return 0


def username_check_str(i: int) -> str:
    if i == 1:
        return "Username too short (minimum 5 characters)"
    if i == 2:
        return "Username too long (maximum 25 characters)"
    if i == 3:
        return "Invalid username: only a-z/A-Z, digits and underscores are accepted"
    if i == 4:
        return "Invalid username: at least 3 letters are required"
    return ""


def check_password(password: str) -> int:
    if len(password) < 8:
        return 1
    if len(password) > 25:
        return 2
    chars = {"lowers": 0, "uppers": 0, "numbers": 0, "special": 0, "others": 0}
    spe = "&#{([-_@)]}°=+*!:/;.,?<>"
    for c in password:
        asc = ord(c)
        if 47 < asc < 58:
            chars["numbers"] += 1
        elif 64 < asc < 91:
            chars["uppers"] += 1
        elif 96 < asc < 123:
            chars["lowers"] += 1
        elif c in spe:
            chars["special"] += 1
        else:
            chars["others"] += 1
    if chars["others"] > 0:
        return 3
    i = 0
    if chars["lowers"] > 0:
        i += 1
    if chars["uppers"] > 0:
        i += 1
    if chars["numbers"] > 0:
        i += 1
    if chars["special"] > 0:
        i += 1
    if i < 3:
        return 4
    return 0


def first_free_id(id_list: list, minimum_id=1) -> int:
    if len(id_list) == 0:
        return minimum_id
    if id_list[-1] == len(id_list) - minimum_id + 1:
        return len(id_list) + minimum_id
    m = 0
    n = len(id_list)-1
    k = True
    while True:
        if n - m == 1:
            return id_list[m]+1
        if n == m:
            return id_list[n]-1 if k else id_list[n]+1
        if id_list[n] - id_list[m + (n-m+1)//2] > id_list[m + (n-m)//2] - id_list[m]:
            k = True
            m += (n-m) // 2
        else:
            n -= (n-m) // 2
            k = False



