import sqlite3


class Constants(object):
    DB_POKEMON = sqlite3.connect("projet/resources/db/pokemon.sqlite", check_same_thread=False)

    @staticmethod
    def get_db_pokemon():
        return Constants.DB_POKEMON
