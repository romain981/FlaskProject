import sys

from .app import app, username_check_str
from .models import *
from flask import render_template, request, send_file, abort
from flask_login import current_user, login_required
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired


@app.context_processor
def log():
    def console_log(message):
        sys.stdout.write(f"{message}\n")
    return dict(log=console_log)


@app.route("/")
def home():
    autologin = None
    autosignup = None
    r = request.args.get("l")
    s = request.args.get("s")
    if r:
        if r == "0":
            autologin = ""
        elif r == "1":
            autologin = "Credentials don't match"
    elif s:
        if s == "0":
            autosignup = "Username credential already used"
        elif int(s) < 5:
            autosignup = username_check_str(int(s))
        elif s == "5":
            autosignup = "Password too short (minimum 8 characters)"
        elif s == "6":
            autosignup = "Password too long (maximum 25 characters)"
        elif s == "7":
            autosignup = "Invalid characters in the password.\nOnly letters, digits and special \
            characters ( &#{([-_@)]}°=+*!:/;.,?<> ) accepted"
        elif s == "8":
            autosignup = "Your password must have at least three of both: lowercase letters, uppercase \
            letters, digits and special characters.\nTolerated special characters are: &#{([-_@)]}°=+*!:/;.,?><"
    return render_template("home.jinja2", pokedex=PokedexEntry.get_pokedex_range(1, 20), l=autologin, s=autosignup)


@app.route("/favicon.ico")
def favicon():
    try:
        return send_file("../resources/static/favicon.ico", mimetype="image/vdn.microsoft.icon", cache_timeout=0)
    except FileNotFoundError as e:
        abort(404)


@app.route("/search")
def search_pokemon():
    pokemons = []
    res = ""
    if request.method == "GET" and request.args.get("q") is not None:
        res = request.args.get("q")
        pokemons = PokedexEntry.search_pokemon(res)
    return render_template("search_pokemon.jinja2", pokemons=pokemons, res=res)


@app.route("/teams/")
@login_required
def show_teams():
    return render_template("teams.jinja2", teamz=current_user.get_teams())


@app.route("/teams/<int:id>")
@login_required
def show_team(id: int):
    team = get_team(id)
    if team.ownerId != current_user.userId:
        abort(403)
    else:
        return render_template("one_team.jinja2", team=team)


@app.route("/pokemon/<int:id>")
@login_required
def one_pokemon(id):
    try:
        pokemon = PokedexEntry.get_pokemon(id)
        return render_template("one_pokemon.jinja2", p=PokedexEntry.get_pokemon(id),
                               moves=pokemon.get_learnable_moves())
    except Exception as e:
        f = open("errors.log", "w+")
        print(e, file=f)
        f.close()
        return ""


class PokedexSearchForm(FlaskForm):
    q = StringField('q', validators=[DataRequired()])


@app.route("/one_type/<int:id>")
def one_type(id):
    return render_template("one_type.jinja2", pokedex_type=PokedexEntry.get_pokedex_for_type(id),
                           type_name=PokedexEntry.get_type(id))
