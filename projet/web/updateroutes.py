import sys
import json
from hashlib import sha256

from .app import app, first_free_id, check_password
from .models import *
from flask import request, redirect
from flask_login import current_user, login_required, logout_user, login_user


@app.route("/_home/<int:lastId>", methods=("GET",))
def updateHome(lastId):
    next_entries = PokedexEntry.get_pokedex_range(lastId + 1, lastId + 20)
    out = "\n".join(
        [f"{p.id};;{p.name};;{p.height};;{p.weight};;{';'.join([t[1] for t in p.types])}" for p in next_entries[:-1]])
    out += f"\n{next_entries[-1]}"
    return out


@app.route("/_team/<int:teamId>", methods=("PATCH",))
@login_required
def updateTeam(teamId):
    team = get_team(teamId)
    if (not team) or team.ownerId != current_user.userId:
        return "2"
    name = request.form.get("name")
    if not name:
        return "2"
    validity = Team.check_name(name)
    if validity[0] == 0:
        team.teamName = name
        db.session.commit()
    return f"{validity[0]};;{validity[1]}"


@app.route("/_team", methods=["DELETE"])
@login_required
def removeTeam():
    teamId = request.form.get("id")
    if not teamId:
        return "2"
    team = get_team(teamId)
    if (not team) or team.ownerId != current_user.userId:
        return "2"
    db.session.delete(team)
    db.session.commit()
    return "0"


@app.route("/_team", methods=["POST"])
@login_required
def addTeam():
    name = request.form.get("name")
    if not name:
        return "3"
    validation = Team.check_name(name)
    if validation[0] > 0:
        return validation[1]
    teams = Team.query.filter(Team.teamName == name).all()
    for team in teams:
        if team.ownerId == current_user.userId:
            return "2"
    query_id = db.session.query(Team.teamId.distinct().label("teamId")).order_by(Team.teamId)
    id_list = [row.teamId for row in query_id.all()]
    new_team_id = first_free_id(id_list)
    new_team = Team(teamId=new_team_id, ownerId=current_user.userId, teamName=name)
    db.session.add(new_team)
    db.session.commit()
    return "0"


@app.route("/_pokemon", methods=['POST'])
def addPokemon():
    p = request.form.get("poke")
    try:
        t = int(request.form.get("team"))
        i = int(request.form.get("index"))
    except ValueError:
        return "2"
    team = get_team(t)
    if not team or team.ownerId != current_user.userId or 1 > i or 6 < i:
        return "2"
    n = PokedexEntry.get_single_pokemon(p)
    if n == 0:
        return "1"
    query_id = db.session.query(Pokemon.pokeId.distinct().label("pokeId")).order_by(Pokemon.pokeId)
    id_list = [row.pokeId for row in query_id.all()]
    new_poke_id = first_free_id(id_list)
    new_poke = Pokemon(pokeId=new_poke_id, ownerId=current_user.userId, pokedexId=n, pokeLevel=1)
    db.session.add(new_poke)
    db.session.commit()
    # noinspection SqlResolve
    db.session.execute(f"update Team set pokemon{i} = {new_poke.pokeId} where teamId = {team.teamId}")
    db.session.commit()
    return "0"


@app.route("/_pokemon/<int:pokeId>", methods=['PATCH'])
@login_required
def updatePokemon(pokeId: int):
    pokemon = get_pokemon(pokeId)
    if not pokemon or pokemon.ownerId != current_user.userId:
        return "-1"
    name = request.form.get("name")
    lev = request.form.get("level")
    attack_number = request.form.get("attackNo")
    attack_id = request.form.get("attackId")
    if name is not None:
        if name == "":
            pokemon.pokeName = None
            db.session.commit()
            return pokemon.get_entry_name()
        pokemon.pokeName = name
        db.session.commit()
        return "1"
    if lev is not None:
        try:
            level = int(lev)
        except ValueError:
            return "2"
        if not 0 < level < 101:
            return "1"
        pokemon.pokeLevel = level
        db.session.commit()
        return "0"
    if attack_number is not None and attack_id is not None:
        if attack_number == "":
            return "1"
        try:
            atknum = int(attack_number)
            atkid = int(attack_id)
            if PokedexMove.get_move(atkid) is None:
                return "3"
            if atknum < 1 or atknum > 4:
                return "2"
            if atknum == 1:
                pokemon.move1 = atkid
            elif atknum == 2:
                pokemon.move2 = atkid
            elif atknum == 3:
                pokemon.move3 = atkid
            else:
                pokemon.move4 = atkid
            db.session.commit()
            return "0"
        except ValueError:
            return "2"
    return "-1"


@app.route("/_pokemon", methods=['DELETE'])
@login_required
def removePokemon():
    pokeId = request.form.get("pokeId")
    try:
        num = int(request.form.get("num"))
    except ValueError:
        return "1"
    pokemon = get_pokemon(pokeId)
    if not pokemon or not num:
        return "2"
    if pokemon.ownerId != current_user.userId or num < 1 or num > 6:
        return "1"
    """team = Team.get_team_with_pokemon(int(pokeId))
    if team is None:
        return "2"
    team.remove_pokemon(pokeId)"""
    # noinspection SqlResolve
    db.engine.execute(f"update Team set pokemon{num} = NULL where pokemon{num} = {pokeId}")
    db.session.commit()
    db.session.delete(pokemon)
    db.session.commit()
    return "0"


@app.route("/_moves", methods=("POST",))
def getPokeMoves():
    form = json.loads(json.dumps(dict(request.form)))
    pokemon = get_pokemon(form["id"])
    if not pokemon or not form["id"]:
        return "Error: Unknown pokemon"
    if pokemon.ownerId != current_user.userId:
        return "Error: Wrong pokemon"
    moves = PokedexEntry.get_pokemon(pokemon.pokedexId).get_learnable_moves_types(form)
    return f"[{','.join([str(i) for i in moves])}]"


@app.route("/_login", methods=["POST"])
def loginRoute():
    username = request.form.get("username")
    password = request.form.get("password")
    nextpage = request.form.get("next")
    user = get_user(username)
    if user is None:
        return redirect("/?l=1")
    m = sha256()
    m.update(password.encode())
    passwd = m.hexdigest()
    if user.passwordHash == passwd:
        sys.stdout.write(f"User {user.username} logged in from {request.remote_addr}\n")
        login_user(user)
        if nextpage and nextpage != "None":
            return redirect(nextpage)
        return redirect("/")
    else:
        return redirect("/?l=1")


@app.route("/_signup", methods=["POST"])
def signupRoute():
    username = request.form.get("username")
    password = request.form.get("password")
    nextpage = request.form.get("next")
    user = get_user(username)
    if user:
        return redirect("/?s=0")
    i = check_username(username)
    if i > 0:
        return redirect(f"/?s={i}")
    j = check_password(password)
    if j > 0:
        return redirect(f"/?s={j + 4}")
    m = sha256()
    m.update(password.encode())
    new_user = User(username=username, passwordHash=m.hexdigest())
    db.session.add(new_user)
    db.session.commit()
    login_user(new_user)
    sys.stdout.write(f"Accounted created: user {new_user.username} with id {new_user.userId} created from "
                     f"{request.remote_addr}\n")
    return redirect("/")


@app.route("/_logout", methods=("POST",))
@login_required
def logoutRoute():
    sys.stdout.write(f"User {current_user.username} logged out from {request.remote_addr}\n")
    logout_user()
    return "0"


@app.route("/_profile", methods=("POST",))
@login_required
def updateProfile():
    user = current_user
    name = request.form.get("name")
    passwd = request.form.get("pass")
    n_passwd = request.form.get("newPass")
    nc_passwd = request.form.get("passConf")
    if passwd is None or sha256(passwd.encode()).hexdigest() != current_user.passwordHash:
        return f"{'{'}\"success\":false, \"err\":1{'}'}"
    name_v = User.check_name(name)
    if name_v[0] != 0 and name != current_user.username and name != "":
        return f"{'{'}\"success\":false, \"err\":0, \"msg\":\"{name_v[1]}\"{'}'}"
    if n_passwd != nc_passwd:
        return f"{'{'}\"success\":false, \"err\":3{'}'}"
    if check_password(n_passwd) != 0 and n_passwd != "":
        return f"{'{'}\"success\":false, \"err\":2{'}'}"
    if name != "" and name != current_user.username:
        current_user.username = name
    if n_passwd != "":
        current_user.passwordHash = sha256(n_passwd.encode()).hexdigest()
    db.session.commit()
    return "{\"success\":true}"


@app.route("/_profile", methods=["DELETE"])
@login_required
def removeProfile():
    user = get_user(current_user.username)
    if user is None: return "-1"
    sys.stdout.write(f"Accounted deleted: user {current_user.username} with id {current_user.userId} removed from "
                     f"{request.remote_addr}\n")
    logout_user()
    db.session.delete(user)
    db.session.commit()
    return "0"
