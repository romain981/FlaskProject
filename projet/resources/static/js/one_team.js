let id = 0;
let typesToggle = {
    normal:   true,
    fighting: true,
    flying:   true,
    poison:   true,
    ground:   true,
    rock:     true,
    bug:      true,
    ghost:    true,
    steel:    true,
    fire:     true,
    water:    true,
    grass:    true,
    electric: true,
    psychic:  true,
    ice:      true,
    dragon:   true,
    dark:     true,
    fairy:    true
};

$(document).ready(function () {
    $('.pokemon_display_ball_img').click(function () {
        addPokemon($(this));
    });
    $('.movesCloseButton').click(function() {
        closeMovesPopup();
    });
    let bgOverlay = $("#movesBgOverlay");
    let popupBase = $("#movesPopupBase");
    bgOverlay.click(function (e) {
        if (mouseOverPopup) return;
        closeMovesPopup();
    });
    bgOverlay.keyup(function (e) {
        if (e.key === "Escape") closeMovesPopup();
    });
    popupBase.mouseover(function () {
        mouseOverPopup = true;
    });
    popupBase.mouseout(function () {
        mouseOverPopup = false;
    });
})

function editName(teamName) {
    let name = prompt("Choose a new name for your team", teamName);
    if (name === null) return;
    $.ajax(`/_team/${id}`, {
        type: 'PATCH',
        dataType: 'html',
        data: {
            "name": name
        },
        success: (msg) => {editNameAnswer(msg, name);}
    });
}

function editNameAnswer(msg, name) {
    if (msg === "0;;") {
        let btn = document.getElementById("name_edit_btn");
        btn.onclick = () => {editName(id, name);};
        let e = document.getElementById("team_name");
        let oldName = e.innerText;
        e.innerText = name;
        swal(`Name change successful\nYour team '${oldName}' is now called '${name}'!`)
    } else if (msg === "2;;") {
        swal(`An error occurred.`);
        location.reload();
    } else {
        let err = msg.split(";;")[1]
        swal(`Error\n${err}`);
    }
}

function removeTeam(teamId, name) {
    id = teamId;
    swal({
        title:`Do you really want to remove the team ${name}?`,
        text: "Warning! This action is not reversible",
        icon: "warning",
        buttons: {
            confirm: {
                text: "Yes, delete it",
                className: "swal2-confirm swal2-styled",
                value: true
            },
            cancel: {
                text: "No",
                className: "swal2-cancel swal2-styled",
                visible: true,
                value: false
            }
        },
        showClass: {
            popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
            popup: 'animate__animated animate__fadeOutUp'
        },
    }).then((result) => {
        if (result === null) return;
        if (result) {
            $.ajax("/_team", {
                type: 'DELETE',
                dataType: 'html',
                data: {
                    "id": id
                },
                success: removeTeamAnswer
            });
        }
    });
}

function removeTeamAnswer(msg) {
    let e = document.getElementById("team_name");
    if (msg === "0") {
        swal(`Team ${e.innerText} removed.`).then(
            _ => window.location.replace("/teams/")
        );
    } else {
        swal(`An error occurred`).then(
            _ => location.reload()
        );
    }
}

function addPokemon(e) {
    let i = e.parent().children()[0].innerText;
    let k = prompt("Enter new pokemon ID or name");
    if (k === null) return;
    $.ajax("/_pokemon", {
        type: 'POST',
        dataType: 'html',
        data: {
            "team": id,
            "poke": k,
            "index": i
        },
        success: addPokemonAnswer
    });
}

function addPokemonAnswer(msg) {
    if (msg === "1") swal({
        title: "Error",
        text:"No pokemon or multiple were found",
        icon: "error",
        timer: 10000
    });
    else if (msg === "2") {
        swal("An error occurred");
        location.reload();
    } else {
        location.reload();
    }
}

function editPokeName(e, pokeId) {
    let name = prompt("Choose a new name for the Pokemon\nLeave blank to remove");
    if (name === null) return;
    $.ajax(`/_pokemon/${pokeId}`, {
        type: 'PATCH',
        dataType: 'html',
        data: {
            'name': name
        },
        success: (msg) => {editPokeNameAnswer(msg, e, name);}
    });
}

function editPokeNameAnswer(msg, e, name) {
    if (msg === "-1") {
        swal("An error occurred");
        location.reload();
    } else if (msg === "1") {
        swal(`Name change to ${name} successful!`);
        e.classList.remove("pokemon_display_species");
        e.classList.add("pokemon_display_name");
        e.innerText = name;
    } else {
        swal("Name removal successful");
        e.classList.remove("pokemon_display_name");
        e.classList.add("pokemon_display_species");
        e.innerText = msg;
    }
}

function editPokeLevel(e, pokeId) {
    let level = prompt("Choose new pokemon level (1~100)");
    if (level === null) return;
    $.ajax(`/_pokemon/${pokeId}`, {
        type: 'PATCH',
        dataType: 'html',
        data: {
            'level': level
        },
        success: (msg) => {editPokeLevelAnswer(msg, e, level);}
    });
}

function editPokeLevelAnswer(msg, e, level) {
    if (msg === "-1") {
        swal("An error occurred");
        location.reload();
    } else if (msg === "2") {
        swal("Level shall be a pure number");
    } else if (msg === "1") {
        swal("Level cannot be below 1 nor over 100");
    } else if (msg === "0") {
        e.innerText = `Level: ${level}`;
    }
}

function remPokemon(pokeId, pokeNum) {
    let valid = confirm(`Do you really want to delete this pokemon?`);
    if (valid) {
        $.ajax("/_pokemon", {
            type: 'DELETE',
            dataType: 'html',
            data: {
                "pokeId": pokeId,
                "num": pokeNum
            },
            success: remPokemonAnswer
        })
    }
}

function remPokemonAnswer(msg) {
    if (msg === "2" || msg === "1") {
        swal("An error occurred.");
    } else {
        swal("Pokemon successfully removed.").then(_ => location.reload);
    }
}

function closeMovesPopup() {
    $("#movesBgOverlay").hide();
    resetToggles();
}

function openMovesPopup(pokeIndex, moveIndex, pokeId, moveId) {
    if (!(pokeIndex >= 0 && moveIndex >= 0 && pokeId > 0)) return;
    $("#movePopupTitle").text(`${$(`#pLabel${pokeIndex}`).text()}'s ${digitStr(moveIndex)} move`);
    let selector = $("#typesSelector");
    let clicked = $(`div[onclick="openMovesPopup(${pokeIndex},${moveIndex},${pokeId},${moveId})"]`);
    selector.empty();
    Object.keys(typesToggle).forEach(k => {
        let type = $(`<input type='text' class='type_p' value='${k}'" readonly>`);
        type.click(function () {
            toggleType(type);
            updateMoves(pokeId, clicked, moveIndex);
        });
        if (typesToggle[k]) type.addClass("selected");
        selector.append(type);
    });
    let actual = $("#actualMove");
    actual.empty();
    if (clicked.hasClass("pokemon_display_attack")) {
        let moveCopy = clicked.clone();
        moveCopy.removeAttr("onclick");
        actual.append(moveCopy);
    }
    updateMoves(pokeId, clicked, moveIndex);
    $("#movesBgOverlay").show();
}

function toggleType(e) {
    if (e.hasClass("selected")) {
        e.removeClass("selected");
        typesToggle[e.val()] = false;
    } else {
        e.addClass("selected");
        typesToggle[e.val()] = true;
    }
}

function updateMoves(pokeId, clicked, moveIndex) {
    $.ajax("/_moves", {
        type: 'POST',
        dataType: 'html',
        data: {
            id: pokeId,
            ...typesToggle
        },
        success: (msg) => {
            if (msg[0] === "E") {
                alert(msg);
                return;
            }
            fillAvailables(msg, clicked, pokeId, moveIndex);
        }
    });
}

function fillAvailables(msg, clicked, pokeId, moveIndex) {
    let moves = Array.from(JSON.parse(msg));
    let availables = $("#availableMoves");
    availables.empty();
    moves.forEach(e => {
        let moveOption = moveDiv(e, null);
        moveOption.click(function () {
            $.ajax(`/_pokemon/${pokeId}`, {
                type: "PATCH",
                dataType: "html",
                data: {
                    "attackNo": moveIndex,
                    "attackId": e.id
                }
            })
            let cop = moveOption.clone();
            cop.attr("onclick", clicked.attr("onclick"));
            clicked.replaceWith(cop);
            closeMovesPopup();
        });
        availables.append(moveOption);
    });
}

function moveDiv(move, movesPopup) {
    let inp = $(`<input type="text" class="type_p" value="${move.type}" readonly/>`);
    let lnk = $(`<a class="link_type" href="/one_type/${move.type_id}"></a>`);
    lnk.append(inp);
    let divType = $(`<div class="pokemon_display_attack_type"></div>`);
    divType.append(lnk);
    let pName = $(`<p class="attack_name">${move.name}</p>`);
    let divName = $("<div class='pokemon_display_attack_name'></div>");
    divName.append(pName);
    let divAtk = $(`<div class="pokemon_display_attack" type="${move.type}"></div>`);
    divAtk.append(divType, divName);
    if (movesPopup !== undefined && movesPopup !== null)
        divAtk.click(function () {
            openMovesPopup(movesPopup[0], movesPopup[1], movesPopup[2], movesPopup[3]);
        });
    return divAtk;
}

function digitStr(i) {
    let out;
    switch (i) {
        case 1:
            out = "1st";
            break;
        case 2:
            out = "2nd";
            break;
        case 3:
            out = "3rd";
            break;
        case 4:
            out = "4th";
            break;
        default:
            out = "";
    }
    return out;
}

function resetToggles() {
    Object.keys(typesToggle).forEach(k => {
        typesToggle[k] = true;
    });
}
