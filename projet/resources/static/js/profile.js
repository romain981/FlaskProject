
function editName() {
    let name = prompt("Choose a new name");
    if (name === null) return;
    $.ajax(`/_profile`, {
        type: 'PATCH',
        dataType: 'html',
        data: {
            "name": name
        },
        success: (msg) => {editNameAnswer(msg, name);}
    })
}



function editNameAnswer(msg, name) {
    if (msg === "0;;") {
        let btn = document.getElementById("name_edit_btn");
        btn.onclick = () => {editName(id, name);};
        let e = document.getElementById("team_name");
        let oldName = e.innerText;
        e.innerText = name;
        alert(`Name change successful\n from ${oldName} to '${name}'!`)
    } else if (msg === "2;;") {
        alert(`An error occurred.`);
        window.location.reload();
    } else {
        let err = msg.split(";;")[1]
        alert(`Error\n${err}`);
    }
}


function removeProfile() {
    let valid = confirm(`Do you really want to delete your account ?\nWarning! This action is not reversible`);
    if (valid) {
        $.ajax(`/_removeProfile`, {
            type: 'POST',
            success: removeProfileAnswer
        })
    }
}


function removeProfileAnswer(msg) {
    let e = document.getElementById("team_name");
    if (msg === "0") {
        alert(`Team ${e.innerText} removed.`)
        window.location.replace("/");
    } else {
        alert(`An error occurred`);
        window.location.reload();
    }
}
