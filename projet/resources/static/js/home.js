function updateDex(msg) {
    let last_row = document.getElementById("last_button_row");
    document.getElementById("home_pokedex_table").lastChild.removeChild(last_row);
    let t = msg.split("\n");
    for (let i = 0; i+1 < t.length; i++) {
        let np = t[i].split(";;");
        let new_row = document.createElement("tr");
        new_row.classList.add("pokemon_row")
        new_row.onclick = () => document.location = `/pokemon/${np[0]}`;
        let types = np[4].split(";");
        let img = document.createElement("img");
        img.setAttribute("src",`/static/images/pokemon/${np[0]}.png`);
        img.setAttribute("alt",`Image de ${np[1]}`);
        let imgCell = document.createElement("td");
        imgCell.appendChild(img);
        new_row.append(imgCell);
        for (let j = 0; j < 4; j++) {
            let cell = document.createElement("td");
            cell.innerText = np[j];
            new_row.appendChild(cell);
        }
        let cell = document.createElement("td");
        for (let j = 0; j < types.length; j++) {
            let type_p = document.createElement("input");
            type_p.setAttribute("type", "text");
            type_p.setAttribute("class","type_p");
            type_p.setAttribute("value", `${types[j]}`);
            type_p.readOnly = true;
            cell.appendChild(type_p);
        }
        new_row.appendChild(cell);
        document.getElementById("home_pokedex_table").lastChild.appendChild(new_row);
        if (i+2 === t.length) {
            document.getElementById("last-dex-id").innerText = np[0];
        }
    }
    if (t[t.length-1]) {
        document.getElementById("home_pokedex_table").lastChild.appendChild(last_row);
    }
}

$(document).ready(function () {
    $('#pokedex_extend').click(function (e) {
        e.preventDefault();
        $('#pokedex_extend').fadeIn("slow");
        setTimeout(function () {
            let lastIndex = document.getElementById("last-dex-id").innerText;
            $.ajax(`/_home/${lastIndex}`, {
                type: "GET",
                dataType: "html",
                success: (data) => {updateDex(data)}
            })
        })
    })
})
