let mouseOverPopup = false;
let username = "";
let userId = "";
let tryDelAcc = false;

/*const profilePopup = new dhx.Popup({
    css: "dhx_profile-popup"
});*/

$(document).ready(() => {
    // login/signup popup listeners
    $('.account_button').click(function() {
        let e = $(this);
        if (e.hasClass("login")) {
            openLoginPopup();
        } else if (e.hasClass("signup")) {
            openSignupPopup();
        }
    });
    $('.popupCloseButton').click(function() {
        clearPopupForm();
    });
    let popupBg = $('.popupBackgroundOverlay');
    let popupBase = $('#baseLoginPopup');
    popupBg.click(function (e) {
        if (mouseOverPopup) return;
        closeLoginPopup();
    });
    popupBg.keyup(function (e) {
        if (e.key === "Escape") clearPopupForm();
    });
    popupBase.mouseover(function () {
        mouseOverPopup = true;
    });
    popupBase.mouseout(function () {
        mouseOverPopup = false;
    });
    // profile popup listeners
    $('.profileCloseButton').click(_ => {
        closeProfilePopup();
    });
    popupBg = $('#profilePopupBgOverlay');
    popupBase = $('#baseProfilePopup');
    popupBg.click(_ => {
        if (mouseOverPopup) return;
        closeProfilePopup();
    });
    popupBg.keyup(function(e) {
        if (e.key === "Escape") closeProfilePopup();
    });
    popupBase.mouseover(_ => {
        mouseOverPopup = true;
    });
    popupBase.mouseout(_ => {
        mouseOverPopup = false;
    });
    username = $('#profileUsername').text() ?? "";
    userId = $('#profileUserId').text() ?? "";
});

function autopopup() {
    if (document.getElementsByClassName("autopopup").length > 0) {
        let errText = document.getElementsByClassName("autopopup")[0].innerText;
        openLoginPopup();
        $('#popupLoginErr').text(errText);
    } else if (document.getElementsByClassName("autosignup").length > 0) {
        let errText = document.getElementsByClassName("autosignup")[0].innerText;
        openSignupPopup();
        $('#popupSignupErr').text(errText);
    }
}

function openLoginPopup() {
    $('.popupBackgroundOverlay').show();
    openTabLogin();
}

function openSignupPopup() {
    $('.popupBackgroundOverlay').show();
    openTabSignup();
}

function closeLoginPopup() {
    $('.popupBackgroundOverlay').hide();
}

function clearPopupForm() {
    closeLoginPopup();
    $('.popupFormInput').val('');
}

function openTabLogin() {
    $('#popupSignupTab').hide();
    $('#popupLoginTab').show();
    $('#popupLoginUsername').focus();
    $('#popupLoginErr').text("");
    setTabBtnActive(document.getElementById('popupLoginTabBtn'));
    setTabBtnInactive(document.getElementById('popupSignupTabBtn'));
}

function openTabSignup() {
    $('#popupLoginTab').hide();
    $('#popupSignupTab').show();
    $('#popupSignupUsername').focus();
    $('#popupSignupErr').text("");
    setTabBtnActive(document.getElementById('popupSignupTabBtn'));
    setTabBtnInactive(document.getElementById('popupLoginTabBtn'));
}

function setTabBtnActive(e) {
    e.style.border = "#00b600 solid 2px";
}

function setTabBtnInactive(e) {
    e.style.border = "gray solid 1px";
}

function loginFormSubmit() {
    let name = $('#popupLoginUsername').val();
    let pass = $('#popupLoginPassword').val();
    $.ajax('/_login', {
        type: "POST",
        dataType: "html",
        data: {
            "username": name,
            "password": pass
        },
        success: (msg) => {loginSubmitAnswer(msg);}
    })
}

function loginSubmitAnswer(msg) {
    if (msg === "0") document.location.reload();
    else {
        $('#popupLoginErr').val(msg);
        $('popupLoginPassword').val('');
    }
}

function signupFormSubmit() {
    let name = $('#popupLoginUsername').val();
    let pass = $('#popupLoginPassword').val();
    $.ajax('/_signup', {
        type: "POST",
        dataType: "html",
        data: {
            "username": name,
            "password": pass
        },
        success: (msg) => {signupSubmitAnswer(msg);}
    })
}

function signupSubmitAnswer(msg) {
    if (msg === "0") document.location.reload();
    else {
        $('#popupLoginErr').val(msg);
        $('popupLoginPassword').val('');
    }
}

function logout() {
    $.ajax('/_logout', {
        type: "POST",
        dataType: "html",
        success: () => {
            window.location.reload();
        }
    })
}

function openProfilePopup() {
    $('#profilePopupBgOverlay').show();
    //profilePopup.show($("#profilePopupBgOverlay"))
}

function closeProfilePopup() {
    $('#profilePopupBgOverlay').hide();
    //profilePopup.hide();
    resetProfilePopup();
}

function setProfilePopupEdit() {
    let pData = $('#profileData');
    pData.empty();
    let pUnameErr = $("<p id='pUnameErr' class='errMarker'></p>");
    let name = $(`<input id="username" class='profileEditInput' placeholder="Username" 
        type='text' value="${username}" required/>`);
    name.on("input", function () {
        name.removeClass("editProfileError");
        pUnameErr.hide();
    })
    let pPassErr = $("<p id='pPassErr' class='errMarker'>Invalid password</p>");
    let password = $(`<input id="password" class="profileEditInput" placeholder="Password" type="password" required/>`);
    password.on("input",function () {
        password.removeClass("editProfileError");
        pPassErr.hide();
    });
    let pConfErr = $("<p id='pConfErr' class='errMarker'></p>");
    let newPass = $("<input id='newPass' class='profileEditInput' placeholder='New password' type='password'/>");
    let passConf = $("<input id='passConf' class='profileEditInput' placeholder='Confirm password' type='password'/>");
    newPass.focusin(function () {
        newPass.removeClass("editProfileError");
        passConf.removeClass("editProfileError");
        pConfErr.hide();
    });
    newPass.focusout(function () {
        if (newPass.val() !== passConf.val()) {
            passConfErr();
        }
    });
    passConf.focusin(function () {
        newPass.removeClass("editProfileError");
        passConf.removeClass("editProfileError");
        pConfErr.hide();
    })
    passConf.focusout(function () {
        if (passConf.val() !== newPass.val()) {
            passConfErr();
        }
    });
    let confirm = $("<input type='submit' value='Save modifications' class='profileEditConfirm pEditBtn profileBtn' " +
        "onclick='tryDelAcc = false;'/>");
    let pVoid = $("<canvas class='profileVoid'></canvas>");
    let profileForm = $("<form class='pEditForm' onsubmit='return submitProfileForm()'></form>");
    let cancel = $("<button class='profileEditCancel pEditBtn profileBtn'>Cancel</button>");
    let buttons = $("<div class='profileButtons'></div>");
    buttons.append(confirm, cancel);
    let del = $("<input type='submit' value='Delete account' class='profileEditDelete pEditBtn profileBtn' " +
        "onclick='tryDelAcc = true;'>");
    profileForm.append(pUnameErr, name, pPassErr, password, pVoid, pConfErr, newPass, passConf,
        pVoid.clone(), buttons, pVoid.clone(), del);
    cancel.click(function () {closeProfilePopup();openProfilePopup()});
    pData.append(profileForm);
}

function submitProfileForm() {
    let newName = $('#username');
    let password = $('#password');
    let newPass = $('#newPass');
    let passConf = $('#passConf');
    if (newName === undefined || password === undefined || newPass === undefined || passConf === undefined) return;
    newName = newName.val() ?? "";
    newName = (newName === username || newName === "") ? username : newName;
    newPass = newPass.val() ?? "";
    passConf = passConf.val() ?? "";
    if (newPass !== passConf) {
        passConfErr();
        return false;
    }
    password = password.val() ?? "";
    $.ajax('/_profile', {
        type: "POST",
        dataType: "html",
        data: {
            "name": newName,
            "pass": password,
            "newPass": newPass,
            "passConf": passConf
        },
        success: (msg) => {
            let res = JSON.parse(msg);
            if (!res.success) {
                if (res.err === 1) {
                    $('#password').addClass("editProfileError");
                    $('#pPassErr')?.show();
                } else if (res.err === 0) {
                    $('#username').addClass("editProfileError");
                    let errMsg = $('#pUnameErr');
                    errMsg.textContent = `Invalid username: ${res.msg}`;
                    errMsg?.show();
                } else {
                    passConfErr();
                    if (res.err === 2) {
                        document.getElementById("pConfErr").innerText = "Invalid passwords";
                    }
                }
                return false;
            }
            if (tryDelAcc) {
                delAcc();
                return false;
            }
            username = newName;
            closeProfilePopup();
            openProfilePopup();
        },
        error: (msg) => {
            swal.error(`Server connection error\n${msg}`);
        }
    });
    return false;
}

function passConfErr() {
    let newPass = $('#newPass');
    let passConf = $('#passConf');
    let pConfErr = $('#pConfErr');
    if (newPass === undefined || passConf === undefined || pConfErr === undefined) return;
    newPass.addClass("editProfileError");
    passConf.addClass("editProfileError");
    pConfErr.textContent = "Passwords do not patch";
    pConfErr.show();
}

function delAcc() {
    if (!tryDelAcc) {
        swal({
            title: "An error occurred",
            icon: "error"
        });
        return false;
    }
    swal({
        title: "Attention!",
        icon: "warning",
        text: "Do you really want to delete your account?\nAll data will be forever lost!",
        className: "pop_top_dark",
        focusConfirm: false,
        buttons: {
            confirm: {
                text: "Yes",
                className: "swal2-confirm swal2-styled",
                value: true
            },
            cancel: {
                text: "No!",
                className: "swal2-cancel swal2-styled",
                value: false,
                visible: true
            }
        },
        showClass: {
            popup: "animate__animated animated__heartbeat"
        },
        hideClass: {
            popup: "animated__animated animate__heartbeat"
        }
    }).then(function(res) {
        if (res !== undefined && res !== null && res) {
            $.ajax("/_profile", {
                type: "DELETE",
                dataType: "html",
                success: () => {
                    location.reload();
                }
            });
        }
    });
}

function resetProfilePopup() {
    let pData = $('#profileData');
    pData.empty();
    let name = $(`<p id="profileUsername" class="profileDataLine">${username}</p>`);
    let nameLab = $("<label for='profileUsername' class='profileLabel'>Username:</label>");
    let id = $(`<p id="profileUserId" class="profileDataLine">${userId}</p>`);
    let idLab = $("<label for='profileUserId' class='profileLabel'>Id:</label>");
    let pVoid = $("<canvas class='profileVoid'></canvas>");
    let pEdit = $("<button class='profileBtn pEditBtn' onclick='setProfilePopupEdit()'>Edit</button>");
    let pLogout = $("<button class='profileBtn pLogoutBtn' onclick='logout()'>Logout</button>");
    let pButtons = $("<div class='profileButtons'></div>");
    pButtons.append(pEdit, pLogout);
    pData.append(nameLab, name, idLab, id, pVoid, pButtons);
}
