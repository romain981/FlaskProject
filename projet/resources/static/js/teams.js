$(document).ready(function () {
    let ids = document.getElementsByClassName("team_display_text");
    for (let i = 0; i < ids.length; i++) {
        let id = ids[i].firstElementChild.innerText
        ids[i].onclick = () => document.location = `./${id}`
    }
})

async function addTeam() {
    let newName = prompt("New team's name");
    if (newName === null) return false;
    $.ajax('/_team', {
        type: 'POST',
        dataType: 'html',
        data: {
            "name": newName
        },
        success: (msg) => {addTeamAnswer(msg, newName);}
    })
}

function addTeamAnswer(msg, name) {
    if (msg === "0") {
        alert(`Team ${name} successfully added.`);
        window.location.reload();
    } else if (msg === "2") {
        alert(`Error: you already have a team named ${name}`);
    } else if (msg === "3") {
        alert("An error occurred");
        window.location.reload();
    } else {
        alert(`Error\n${msg}`);
    }
}
