::==================================
:: Windows Flask app launch script
:: @code : Marc IDIR
:: @testing : Melissa MARCO
::==================================

@echo off

echo Python Flask Pokedex app
echo ~~~~~~~~~~~~~~~~~~~

if not exist winvenv\Scripts\activate (
  echo
  choice /c yn /d n /t 15 /m "Create a new virtualenv?"
  if %errorlevel% == 2 (
    echo Exiting.
    exit \b 0
  )
  setlocal enableextensions
  for /F "tokens=* USEBACKQ" %%F IN (`where python.exe`) DO (
    set pythonpath=%%F
  )
  echo %pythonpath%
  virtualenv --python %pythonpath% winvenv
  endlocal
  set /P "port=Choose server port > "
  if "%1" == "open" (
    .\winvenv\Scripts\activate & echo. & pip install -r requirements.txt & flask run -h 0.0.0.0 -p %port%
  ) else (
    .\winvenv\Scripts\activate & echo. & pip install -r requirements.txt & flask run -p %port%
  )
) else (
  set /p "port=Choose server port > "
  if "%1" == "open" (
    .\winvenv\Scripts\activate & flask run -h 0.0.0.0 -p %port%
  ) else (
    .\winvenv\Scripts\activate & flask run -p %port%
  )
)
